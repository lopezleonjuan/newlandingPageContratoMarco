$(document).ready(function () {
    
    function hover(text){
        return function(){
            $(text).hide();
        }
    } 

    function leave(text){
        return function() {
            $(text).show();
        }
    }

    $('.card1').hover(hover('.text1'), leave('.text1'));
    $('.card2').hover(hover('.text2'), leave('.text2'));
    $('.card3').hover(hover('.text3'), leave('.text3'));
    $('.card4').hover(hover('.text4'), leave('.text4'));
    $('.card5').hover(hover('.text5'), leave('.text5'));
    $('.card6').hover(hover('.text6'), leave('.text6'));
    $('.card7').hover(hover('.text7'), leave('.text7'));
    $('.card8').hover(hover('.text8'), leave('.text8'));
    $('.card9').hover(hover('.text9'), leave('.text9'));
    $('.card10').hover(hover('.text10'), leave('.text10'));
    $('.card11').hover(hover('.text11'), leave('.text11'));
    $('.card12').hover(hover('.text12'), leave('.text12'));
    $('.card13').hover(hover('.text13'), leave('.text13'));
    $('.card14').hover(hover('.text14'), leave('.text14'));

})